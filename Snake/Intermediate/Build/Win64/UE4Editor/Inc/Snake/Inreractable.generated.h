// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_Inreractable_generated_h
#error "Inreractable.generated.h already included, missing '#pragma once' in Inreractable.h"
#endif
#define SNAKE_Inreractable_generated_h

#define Snake_Source_Snake_Inreractable_h_13_SPARSE_DATA
#define Snake_Source_Snake_Inreractable_h_13_RPC_WRAPPERS
#define Snake_Source_Snake_Inreractable_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_Source_Snake_Inreractable_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKE_API UInreractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInreractable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKE_API, UInreractable); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInreractable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKE_API UInreractable(UInreractable&&); \
	SNAKE_API UInreractable(const UInreractable&); \
public:


#define Snake_Source_Snake_Inreractable_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKE_API UInreractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKE_API UInreractable(UInreractable&&); \
	SNAKE_API UInreractable(const UInreractable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKE_API, UInreractable); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInreractable); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInreractable)


#define Snake_Source_Snake_Inreractable_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUInreractable(); \
	friend struct Z_Construct_UClass_UInreractable_Statics; \
public: \
	DECLARE_CLASS(UInreractable, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/Snake"), SNAKE_API) \
	DECLARE_SERIALIZER(UInreractable)


#define Snake_Source_Snake_Inreractable_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Snake_Source_Snake_Inreractable_h_13_GENERATED_UINTERFACE_BODY() \
	Snake_Source_Snake_Inreractable_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_Inreractable_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Snake_Source_Snake_Inreractable_h_13_GENERATED_UINTERFACE_BODY() \
	Snake_Source_Snake_Inreractable_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_Inreractable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IInreractable() {} \
public: \
	typedef UInreractable UClassType; \
	typedef IInreractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Snake_Source_Snake_Inreractable_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~IInreractable() {} \
public: \
	typedef UInreractable UClassType; \
	typedef IInreractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Snake_Source_Snake_Inreractable_h_10_PROLOG
#define Snake_Source_Snake_Inreractable_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_Inreractable_h_13_SPARSE_DATA \
	Snake_Source_Snake_Inreractable_h_13_RPC_WRAPPERS \
	Snake_Source_Snake_Inreractable_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_Inreractable_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_Inreractable_h_13_SPARSE_DATA \
	Snake_Source_Snake_Inreractable_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_Inreractable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class UInreractable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_Inreractable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
